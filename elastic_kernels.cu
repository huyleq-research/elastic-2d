#include "elastic_kernels.h"

__global__ void inject_source(float *sx,float *sz,float dtsou,int slocxz){
 sx[slocxz]+=dtsou;
 sz[slocxz]+=dtsou;
 return;
}

__global__ void inject_dipole_source(float *sx,float *sz,float dtsou,int slocxz,int nx){
 sx[slocxz]+=dtsou;
 sz[slocxz]+=dtsou;
 sx[slocxz+nx]-=dtsou;
 sz[slocxz+nx]-=dtsou;
 return;
}

__global__ void update_vel(float *rho,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,int npad,float dx,float dy,float dt){
                                  
 __shared__ float s_sx[BLOCK_DIM][BLOCK_DIM+2*HALF_STENCIL-1],
                  s_sz[BLOCK_DIM+2*HALF_STENCIL-1][BLOCK_DIM],
                  s_sxz[BLOCK_DIM+2*HALF_STENCIL-1][BLOCK_DIM+2*HALF_STENCIL-1];

 int ix=blockIdx.x*blockDim.x + threadIdx.x;
 int iy=blockIdx.y*blockDim.y + threadIdx.y;

 int s_ix=threadIdx.x+HALF_STENCIL;
 int s_iy=threadIdx.y+HALF_STENCIL;
  
 s_sx[threadIdx.y][s_ix]=sx[iy*nx+ix];
 s_sz[s_iy][threadIdx.x]=sz[iy*nx+ix];
 s_sxz[s_iy-1][s_ix-1]=sxz[iy*nx+ix];
 
 if(threadIdx.x<HALF_STENCIL){
  int l=(blockIdx.x!= 0);
  int r=(blockIdx.x!=gridDim.x-1);
  s_sx[threadIdx.y][threadIdx.x]=l*sx[l*(iy*nx+ix-HALF_STENCIL)];
  s_sxz[s_iy-1][s_ix+blockDim.x-1]=r*sxz[r*(iy*nx+ix+blockDim.x)];
  if(threadIdx.x!=HALF_STENCIL-1){
   s_sx[threadIdx.y][s_ix+blockDim.x]=r*sx[r*(iy*nx+ix+blockDim.x)];
   s_sxz[s_iy-1][threadIdx.x]=l*sxz[l*(iy*nx+ix-HALF_STENCIL+1)];
  }
 }  
 if(threadIdx.y<HALF_STENCIL){
  int t=(blockIdx.y!=0);
  int b=(blockIdx.y!=gridDim.y-1);
  s_sz[threadIdx.y][threadIdx.x]=t*sz[t*((iy-HALF_STENCIL)*nx+ix)];
  s_sxz[s_iy+blockDim.y-1][s_ix-1]=b*sxz[b*((iy+blockDim.y)*nx+ix)];
  if(threadIdx.y != HALF_STENCIL-1){
   s_sz[s_iy+blockDim.y][threadIdx.x]=b*sz[b*((iy+blockDim.y)*nx+ix)];
   s_sxz[threadIdx.y][s_ix-1]=t*sxz[t*((iy-HALF_STENCIL+1)*nx+ix)];
  }
 }
 
  __szncthreads();
    
 float vx1=0.f,vz1=0.f;
 for(int i=0;i<HALF_STENCIL;i++){
  vx1+=c[i]*((s_sx[threadIdx.y][s_ix+i]-s_sx[threadIdx.y][s_ix-i-1])/dx+
             (s_sxz[s_iy-1+i+1][s_ix-1]-s_sxz[s_iy-1-i][s_ix-1])/dy);
  vz1+=c[i]*((s_sxz[s_iy-1][s_ix-1+i+1]-s_sxz[s_iy-1][s_ix-1-i])/dx+
             (s_sz[s_iy+i][threadIdx.x]-s_sz[s_iy-i-1][threadIdx.x])/dy);                                  
 }      
 vx[iy*nx+ix]=dt/rho[iy*nx+ix]*vx1+vx[iy*nx+ix]; 
 vz[iy*nx+ix]=dt/rho[iy*nx+ix]*vz1+vz[iy*nx+ix];                                    

 if(ix<npad){
   vx[iy*nx+ix]*=taper[ix];
   vz[iy*nx+ix]*=taper[ix];
   vx[iy*nx+nx-1-ix]*=taper[ix];
   vz[iy*nx+nx-1-ix]*=taper[ix]; 
 }
 
 if(iy<npad){
  vx[iy*nx+ix]*=taper[iy];
  vz[iy*nx+ix]*=taper[iy];
  vx[(ny-1-iy)*nx+ix]*=taper[iy];
  vz[(ny-1-iy)*nx+ix]*=taper[iy]; 
 }
 return;
}


__global__ void update_stress(float *lam,float *mu,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,float dx,float dy,float dt){  
 
 __shared__ float s_vx[BLOCK_DIM+2*HALF_STENCIL-1][BLOCK_DIM+2*HALF_STENCIL-1],
                  s_vz[BLOCK_DIM+2*HALF_STENCIL-1][BLOCK_DIM+2*HALF_STENCIL-1];
                   
 int ix=blockIdx.x*blockDim.x + threadIdx.x;
 int iy=blockIdx.y*blockDim.y + threadIdx.y;

 int s_ix=threadIdx.x+HALF_STENCIL;
 int s_iy=threadIdx.y+HALF_STENCIL;
  
 s_vx[s_iy][s_ix-1]=vx[iy*nx+ix];
 s_vz[s_iy-1][s_ix]=vz[iy*nx+ix];
 
 if(threadIdx.x < HALF_STENCIL){
  int l=(blockIdx.x != 0);
  int r=(blockIdx.x != gridDim.x-1); 
  s_vx[s_iy][s_ix+blockDim.x-1]=r*vx[r*(iy*nx+ix+blockDim.x)];
  s_vz[s_iy-1][threadIdx.x]=l*vz[l*(iy*nx+ix-HALF_STENCIL)];
  if(threadIdx.x != HALF_STENCIL-1){
   s_vx[s_iy][threadIdx.x]=l*vx[l*(iy*nx+ix-HALF_STENCIL+1)];
   s_vz[s_iy-1][s_ix+blockDim.x]=r*vz[r*(iy*nx+ix+blockDim.x)];
  }
 }

 if(threadIdx.y < HALF_STENCIL){
  int t=(blockIdx.y != 0);
  int b=(blockIdx.y != gridDim.y-1);
  s_vx[threadIdx.y][s_ix-1]=t*vx[t*((iy-HALF_STENCIL)*nx+ix)];
  s_vz[s_iy+blockDim.y-1][s_ix]=b*vz[b*((iy+blockDim.y)*nx+ix)];
  if(threadIdx.y != HALF_STENCIL-1){
   s_vx[s_iy+blockDim.y][s_ix-1]=b*vx[b*((iy+blockDim.y)*nx+ix)];
   s_vz[threadIdx.y][s_ix]=t*vz[t*((iy-HALF_STENCIL+1)*nx+ix)];
  }
 }
 
 __szncthreads();
   
 float dvxdx=0.f,dvzdy=0.f,sxz1=0.f;
 for(int i=0;i<HALF_STENCIL;i++){
  dvxdx+=c[i]*(s_vx[s_iy][s_ix-1+i+1]-s_vx[s_iy][s_ix-1-i])/dx;
  dvzdy+=c[i]*(s_vz[s_iy-1+i+1][s_ix]-s_vz[s_iy-1-i][s_ix])/dy;
  sxz1+=c[i]*((s_vz[s_iy-1][s_ix+i]-s_vz[s_iy-1][s_ix-i-1])/dx+
              (s_vx[s_iy+i][s_ix-1]-s_vx[s_iy-i-1][s_ix-1])/dy);             
 }   
 sx[iy*nx+ix]=dt*((lam[iy*nx+ix]+2.f*mu[iy*nx+ix])*dvxdx+lam[iy*nx+ix]*dvzdy)+sx[iy*nx+ix];
 sz[iy*nx+ix]=dt*((lam[iy*nx+ix]+2.f*mu[iy*nx+ix])*dvzdy+lam[iy*nx+ix]*dvxdx)+sz[iy*nx+ix];
 sxz[iy*nx+ix]=dt*mu[iy*nx+ix]*sxz1+sxz[iy*nx+ix];

 if(ix<npad){
  sx[iy*nx+ix]*=taper[ix];
  sz[iy*nx+ix]*=taper[ix];
  sxz[iy*nx+ix]*=taper[ix];
  sx[iy*nx+nx-1-ix]*=taper[ix];
  sz[iy*nx+nx-1-ix]*=taper[ix]; 
  sxz[iy*nx+nx-1-ix]*=taper[ix]; 
 }
 
 if(iy<npad){
  sx[iy*nx+ix]*=taper[iy];
  sz[iy*nx+ix]*=taper[iy];
  sxz[iy*nx+ix]*=taper[iy];
  sx[(ny-1-iy)*nx+ix]*=taper[iy];
  sz[(ny-1-iy)*nx+ix]*=taper[iy]; 
  sxz[(ny-1-iy)*nx+ix]*=taper[iy];
 }
 return;
}

