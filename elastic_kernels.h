#ifndef ELASTIC_KERNELS_H
#define ELASTIC_KERNELS_H

__global__ void inject_source(float *sx,float *sz,float dtsou,int slocxz);

__global__ void inject_dipole_source(float *sx,float *sz,float dtsou,int slocxz,int nx);

__global__ void update_vel(float *rho,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,float dx,float dy,float dt);

__global__ void update_stress(float *lam,float *mu,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,int npad,float dx,float dy,float dt);

#endif
