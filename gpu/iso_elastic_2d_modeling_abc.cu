#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda_runtime_api.h>

#include "hutil.h"

#define BLOCK_DIM_X 13
#define BLOCK_DIM_Y 13
#define RADIUS 4
#define NPAD 50

__constant__ float c[RADIUS],xytaper[NPAD];

float const pi=4.*atan(1.);

void init_stencil(float **aa,float *a);

__global__ void snap_shot(float *d_p,float *d_ux,float *d_uy,float *sx,float *sy,float *ux,float *uy,int nx);

__global__ void inject(float *sx,float *sy,int nx,float sou,int islocx,int islocy);
                                                                 
__global__ void update_vel(float *rho,float *sx,float *sy,float *sxy,float *vx,float *vy,
                                  int nx,int ny,float dx,float dy,float dt);
                          
__global__ void update_stress(float *lam,float *mu,float *sx,float *sy,float *sxy,float *vx,float *vy,
                                     int nx,int ny,float dx,float dy,float dt);
                          
                                                                                                        
int main(int argc, char* argv[]){

 cudaSetDevice(0);
 
 
 g_argc=argc;
 g_argv=argv;
 
 
 //init parameters
 int ny,nx,nt;
 float dy,dx,dt,oy,ox,ot; 
 hscanf("oy",&oy);
 hscanf("ox",&ox);
 hscanf("ot",&ot);
 hscanf("dy",&dy);
 hscanf("dx",&dx);
 hscanf("dt",&dt);
 hscani("ny",&ny);
 hscani("nx",&nx);
 hscani("nt",&nt);
  
  
 //init stencil 
 float *a,**aa;
 a=(float *)malloc(36*sizeof(float));
 aa=(float **)malloc(9*sizeof(float *));
 
 init_stencil(aa,a);
 
 cudaMemcpyToSymbol(c,aa[RADIUS],RADIUS*sizeof(float));
 
 
 //init source
 float slocy,slocx,f;
 hscanf("slocy",&slocy);
 hscanf("slocx",&slocx);
 hscanf("f",&f);
 
 float tdelay=1.2f/f;
 int islocy=(int) floor((slocy-oy)/dy+0.5f);
 int islocx=(int) floor((slocx-ox)/dx+0.5f);

 float *sou;
 sou=(float *) malloc(nt*sizeof(float));
 for(int i=0;i<nt;i++){
  sou[i]=(1.f-2.f*pi*pi*f*f*(i*dt-tdelay)*(i*dt-tdelay))*exp(-pi*pi*f*f*(i*dt-tdelay)*(i*dt-tdelay));
 }
 
 
 //init abc
 float *xytaper1;
 xytaper1=(float *) malloc(NPAD*sizeof(float));
 for(int i=0;i<NPAD;i++) xytaper1[i]=0.9f+0.1f*cos(pi*(NPAD-i-1)/NPAD);

 cudaMemcpyToSymbol(xytaper,xytaper1,NPAD*sizeof(float));
 
  
 //init models
 float *lam,*mu,*rho;
 lam=(float *) malloc(ny*nx*sizeof(float));
 mu=(float *) malloc(ny*nx*sizeof(float));
 rho=(float *) malloc(ny*nx*sizeof(float));
 
 hread("lam",lam,ny*nx);
 hread("mu",mu,ny*nx); 
 hread("rho",rho,ny*nx); 
     
 float *d_lam,*d_mu,*d_rho;
 cudaMalloc((void**)&d_lam,ny*nx*sizeof(float));
 cudaMalloc((void**)&d_mu,ny*nx*sizeof(float)); 
 cudaMalloc((void**)&d_rho,ny*nx*sizeof(float));
 
 cudaMemcpy(d_lam,lam,ny*nx*sizeof(float),cudaMemcpyHostToDevice);     
 cudaMemcpy(d_mu,mu,ny*nx*sizeof(float),cudaMemcpyHostToDevice);
 cudaMemcpy(d_rho,rho,ny*nx*sizeof(float),cudaMemcpyHostToDevice);

 
 //init field variables and data on device global memory
 float rate;
 hscanf("rate",&rate);
 int leap=(int) floor(rate/dt+0.5f);
 
 float *sy,*sx,*sxy,*vy,*vx;
 cudaMalloc((void**)&sx,ny*nx*sizeof(float));
 cudaMalloc((void**)&sy,ny*nx*sizeof(float));
 cudaMalloc((void**)&sxy,ny*nx*sizeof(float));
 cudaMalloc((void**)&vx,ny*nx*sizeof(float));
 cudaMalloc((void**)&vy,ny*nx*sizeof(float));
 
 cudaMemset(sx,0,ny*nx*sizeof(float));
 cudaMemset(sy,0,ny*nx*sizeof(float));
 cudaMemset(sxy,0,ny*nx*sizeof(float));
 cudaMemset(vx,0,ny*nx*sizeof(float));
 cudaMemset(vy,0,ny*nx*sizeof(float));
 
 
 //output wavefields
 float *d_p,*d_ux,*d_uy;
 cudaMalloc((void**)&d_p,ny*nx*sizeof(float));
 cudaMalloc((void**)&d_ux,ny*nx*sizeof(float));
 cudaMalloc((void**)&d_uy,ny*nx*sizeof(float));
 
 int nnt=(int) floor((nt-1.f)*dt/rate+1.5f);
 float *p,*uy,*ux;
 cudaHostAlloc((void**)&p,nnt*ny*nx*sizeof(float),cudaHostAllocDefault);
 cudaHostAlloc((void**)&uy,nnt*ny*nx*sizeof(float),cudaHostAllocDefault);
 cudaHostAlloc((void**)&ux,nnt*ny*nx*sizeof(float),cudaHostAllocDefault);
 
 
 //grid configuration
 dim3 block(BLOCK_DIM_X,BLOCK_DIM_Y);
 dim3 grid(nx/BLOCK_DIM_X,ny/BLOCK_DIM_Y);
 
 
 //stream create
 cudaStream_t comp_stream,output_stream;
 cudaStreamCreate(&comp_stream);
 cudaStreamCreate(&output_stream);
 
 
 //time loop
 for(int it=0;it<nt;it++){
  printf("time step %d \n", it);  

  inject<<<1,1,0,comp_stream>>>(sx,sy,nx,sou[it],islocx,islocy);
                                    
  if(it%leap == 0){
   cudaStreamSynchronize(output_stream); 
   snap_shot<<<grid,block,0,comp_stream>>>(d_p,d_ux,d_uy,sx,sy,vx,vy,nx);
   cudaStreamSynchronize(comp_stream);

   float *p_pt,*ux_pt,*uy_pt;
   p_pt=p+it/leap*ny*nx;
   ux_pt=ux+it/leap*ny*nx;
   uy_pt=uy+it/leap*ny*nx;
   cudaMemcpyAsync(p_pt,d_p,ny*nx*sizeof(float),cudaMemcpyDeviceToHost,output_stream); 
   cudaMemcpyAsync(ux_pt,d_ux,ny*nx*sizeof(float),cudaMemcpyDeviceToHost,output_stream); 
   cudaMemcpyAsync(uy_pt,d_uy,ny*nx*sizeof(float),cudaMemcpyDeviceToHost,output_stream);    
  }
  
  update_vel<<<grid,block,0,comp_stream>>>(d_rho,sx,sy,sxy,vx,vy,nx,ny,dx,dy,dt); 
  update_stress<<<grid,block,0,comp_stream>>>(d_lam,d_mu,sx,sy,sxy,vx,vy,nx,ny,dx,dy,dt);                                                                                                                                  
 }
 
 
 //stream destroy
 cudaStreamDestroy(comp_stream);
 cudaStreamDestroy(output_stream);
  
 
 //write out data
 hwrite("p",p,nnt*ny*nx);
 headerf("p","o1",&ox);
 headerf("p","o2",&oy);
 headerf("p","o3",&ot);
 headerf("p","d1",&dx);
 headerf("p","d2",&dy);
 headerf("p","d3",&rate);
 headeri("p","n1",&nx);
 headeri("p","n2",&ny);
 headeri("p","n3",&nnt);

 ot-=dt/2.f;
 hwrite("uy",uy,nnt*ny*nx);
 headerf("uy","o1",&ox);
 headerf("uy","o2",&oy);
 headerf("uy","o3",&ot);
 headerf("uy","d1",&dx);
 headerf("uy","d2",&dy);
 headerf("uy","d3",&rate);
 headeri("uy","n1",&nx);
 headeri("uy","n2",&ny);
 headeri("uy","n3",&nnt);

 hwrite("ux",ux,nnt*ny*nx);
 headerf("ux","o1",&ox);
 headerf("ux","o2",&oy);
 headerf("ux","o3",&ot);
 headerf("ux","d1",&dx);
 headerf("ux","d2",&dy);
 headerf("ux","d3",&rate);
 headeri("uy","n1",&nx);
 headeri("uy","n2",&ny);
 headeri("ux","n3",&nnt);
  
 free(sou);free(lam);free(mu);free(rho);free(a);free(aa);free(xytaper1);
 cudaFreeHost(p);cudaFreeHost(uy);cudaFreeHost(ux);
 cudaFree(d_lam);cudaFree(d_mu);cudaFree(d_rho);cudaFree(d_p);cudaFree(d_ux);cudaFree(d_uy);
 cudaFree(sx);cudaFree(sy);cudaFree(sxy);cudaFree(vx);cudaFree(vy);
}


void init_stencil(float **aa,float *a){
 a[0]=1.f;

 a[1]=9.f/8.f;
 a[2]=-1.f/24.f;
  
 a[3]=75.f/64.f;
 a[4]=-25.f/384.f;
 a[5]=3.f/640.f;
  
 a[6]=1225.f/1024.f;
 a[7]=-245.f/3072.f;
 a[8]=49.f/5120.f;
 a[9]=-5.f/7168.f;

 a[10]=1.211242675781250f;
 a[11]=-0.089721679687500f;
 a[12]=0.013842773437500f;
 a[13]=-0.001765659877232f;
 a[14]=0.000118679470486f;

 a[15]=1.221336364746094f; 
 a[16]=-0.096931457519531f;
 a[17]=0.017447662353516f;
 a[18]=-0.002967289515904f;
 a[19]=0.000359005398220f;
 a[20]=-0.000021847811612f;

 a[21]=1.228606224060059f;
 a[22]=-0.102383852005005f;
 a[23]=0.020476770401001f;
 a[24]=-0.004178932734898f;
 a[25]=0.000689453548855f;
 a[26]=-0.000076922503385f;
 a[27]=0.000004236514752f;
 
 a[28]=1.234091073274612f;
 a[29]=-0.106649845838547f;
 a[30]=0.023036366701126f;
 a[31]=-0.005342385598591f;
 a[32]=0.001077271170086f;
 a[33]=-0.000166418877515f;
 a[34]=0.000017021711056f;
 a[34]=-0.000000852346420f;
  
 aa[1]=a;
 aa[2]=a+1;
 aa[3]=a+3;
 aa[4]=a+6;
 aa[5]=a+10;
 aa[6]=a+15;
 aa[7]=a+21;
 aa[8]=a+28; 
 
}


__global__ void inject(float *sx,float *sy,int nx,float sou,int islocx,int islocy){
  sx[islocy*nx+islocx]+=sou;
  sy[islocy*nx+islocx]+=sou;
}


__global__ void snap_shot(float *d_p,float *d_ux,float *d_uy,float *sx,float *sy,float *vx,float *vy,int nx){
 int ix=blockIdx.x*blockDim.x + threadIdx.x;
 int iy=blockIdx.y*blockDim.y + threadIdx.y;
 d_p[iy*nx+ix]=0.5f*(sx[iy*nx+ix]+sy[iy*nx+ix]);
 d_ux[iy*nx+ix]=vx[iy*nx+ix];
 d_uy[iy*nx+ix]=vy[iy*nx+ix];
}


__global__ void update_vel(float *rho,float *sx,float *sy,float *sxy,float *vx,float *vy,
                                  int nx,int ny,float dx,float dy,float dt){
                                  
 __shared__ float s_sx[BLOCK_DIM_Y][BLOCK_DIM_X+2*RADIUS-1],
                  s_sy[BLOCK_DIM_Y+2*RADIUS-1][BLOCK_DIM_X],
                  s_sxy[BLOCK_DIM_Y+2*RADIUS-1][BLOCK_DIM_X+2*RADIUS-1];

 int ix=blockIdx.x*blockDim.x + threadIdx.x;
 int iy=blockIdx.y*blockDim.y + threadIdx.y;

 int s_ix=threadIdx.x+RADIUS;
 int s_iy=threadIdx.y+RADIUS;
  
 s_sx[threadIdx.y][s_ix]=sx[iy*nx+ix];
 s_sy[s_iy][threadIdx.x]=sy[iy*nx+ix];
 s_sxy[s_iy-1][s_ix-1]=sxy[iy*nx+ix];
 
 if(threadIdx.x < RADIUS){
  int l=(blockIdx.x != 0);
  int r=(blockIdx.x != gridDim.x-1);
  s_sx[threadIdx.y][threadIdx.x]=l*sx[l*(iy*nx+ix-RADIUS)];
  s_sxy[s_iy-1][s_ix+blockDim.x-1]=r*sxy[r*(iy*nx+ix+blockDim.x)];
  if(threadIdx.x != RADIUS-1){
   s_sx[threadIdx.y][s_ix+blockDim.x]=r*sx[r*(iy*nx+ix+blockDim.x)];
   s_sxy[s_iy-1][threadIdx.x]=l*sxy[l*(iy*nx+ix-RADIUS+1)];
  }
 }  
 if(threadIdx.y < RADIUS){
  int t=(blockIdx.y != 0);
  int b=(blockIdx.y != gridDim.y-1);
  s_sy[threadIdx.y][threadIdx.x]=t*sy[t*((iy-RADIUS)*nx+ix)];
  s_sxy[s_iy+blockDim.y-1][s_ix-1]=b*sxy[b*((iy+blockDim.y)*nx+ix)];
  if(threadIdx.y != RADIUS-1){
   s_sy[s_iy+blockDim.y][threadIdx.x]=b*sy[b*((iy+blockDim.y)*nx+ix)];
   s_sxy[threadIdx.y][s_ix-1]=t*sxy[t*((iy-RADIUS+1)*nx+ix)];
  }
 }
 
  __syncthreads();
    
 float vx1=0.f,vy1=0.f;
 for(int i=0;i<RADIUS;i++){
  vx1+=c[i]*((s_sx[threadIdx.y][s_ix+i]-s_sx[threadIdx.y][s_ix-i-1])/dx+
             (s_sxy[s_iy-1+i+1][s_ix-1]-s_sxy[s_iy-1-i][s_ix-1])/dy);
  vy1+=c[i]*((s_sxy[s_iy-1][s_ix-1+i+1]-s_sxy[s_iy-1][s_ix-1-i])/dx+
             (s_sy[s_iy+i][threadIdx.x]-s_sy[s_iy-i-1][threadIdx.x])/dy);                                  
 }      
 vx[iy*nx+ix]=dt/rho[iy*nx+ix]*vx1+vx[iy*nx+ix]; 
 vy[iy*nx+ix]=dt/rho[iy*nx+ix]*vy1+vy[iy*nx+ix];                                    

 if(ix<NPAD){
   vx[iy*nx+ix]*=xytaper[ix];
   vy[iy*nx+ix]*=xytaper[ix];
   vx[iy*nx+nx-1-ix]*=xytaper[ix];
   vy[iy*nx+nx-1-ix]*=xytaper[ix]; 
 }
 
 if(iy<NPAD){
  vx[iy*nx+ix]*=xytaper[iy];
  vy[iy*nx+ix]*=xytaper[iy];
  vx[(ny-1-iy)*nx+ix]*=xytaper[iy];
  vy[(ny-1-iy)*nx+ix]*=xytaper[iy]; 
 }
}


__global__ void update_stress(float *lam,float *mu,float *sx,float *sy,float *sxy,float *vx,float *vy,
                                     int nx,int ny,float dx,float dy,float dt){  
 
 __shared__ float s_vx[BLOCK_DIM_Y+2*RADIUS-1][BLOCK_DIM_X+2*RADIUS-1],
                  s_vy[BLOCK_DIM_Y+2*RADIUS-1][BLOCK_DIM_X+2*RADIUS-1];
                   
 int ix=blockIdx.x*blockDim.x + threadIdx.x;
 int iy=blockIdx.y*blockDim.y + threadIdx.y;

 int s_ix=threadIdx.x+RADIUS;
 int s_iy=threadIdx.y+RADIUS;
  
 s_vx[s_iy][s_ix-1]=vx[iy*nx+ix];
 s_vy[s_iy-1][s_ix]=vy[iy*nx+ix];
 
 if(threadIdx.x < RADIUS){
  int l=(blockIdx.x != 0);
  int r=(blockIdx.x != gridDim.x-1); 
  s_vx[s_iy][s_ix+blockDim.x-1]=r*vx[r*(iy*nx+ix+blockDim.x)];
  s_vy[s_iy-1][threadIdx.x]=l*vy[l*(iy*nx+ix-RADIUS)];
  if(threadIdx.x != RADIUS-1){
   s_vx[s_iy][threadIdx.x]=l*vx[l*(iy*nx+ix-RADIUS+1)];
   s_vy[s_iy-1][s_ix+blockDim.x]=r*vy[r*(iy*nx+ix+blockDim.x)];
  }
 }

 if(threadIdx.y < RADIUS){
  int t=(blockIdx.y != 0);
  int b=(blockIdx.y != gridDim.y-1);
  s_vx[threadIdx.y][s_ix-1]=t*vx[t*((iy-RADIUS)*nx+ix)];
  s_vy[s_iy+blockDim.y-1][s_ix]=b*vy[b*((iy+blockDim.y)*nx+ix)];
  if(threadIdx.y != RADIUS-1){
   s_vx[s_iy+blockDim.y][s_ix-1]=b*vx[b*((iy+blockDim.y)*nx+ix)];
   s_vy[threadIdx.y][s_ix]=t*vy[t*((iy-RADIUS+1)*nx+ix)];
  }
 }
 
 __syncthreads();
   
 float dvxdx=0.f,dvydy=0.f,sxy1=0.f;
 for(int i=0;i<RADIUS;i++){
  dvxdx+=c[i]*(s_vx[s_iy][s_ix-1+i+1]-s_vx[s_iy][s_ix-1-i])/dx;
  dvydy+=c[i]*(s_vy[s_iy-1+i+1][s_ix]-s_vy[s_iy-1-i][s_ix])/dy;
  sxy1+=c[i]*((s_vy[s_iy-1][s_ix+i]-s_vy[s_iy-1][s_ix-i-1])/dx+
              (s_vx[s_iy+i][s_ix-1]-s_vx[s_iy-i-1][s_ix-1])/dy);             
 }   
 sx[iy*nx+ix]=dt*((lam[iy*nx+ix]+2.f*mu[iy*nx+ix])*dvxdx+lam[iy*nx+ix]*dvydy)+sx[iy*nx+ix];
 sy[iy*nx+ix]=dt*((lam[iy*nx+ix]+2.f*mu[iy*nx+ix])*dvydy+lam[iy*nx+ix]*dvxdx)+sy[iy*nx+ix];
 sxy[iy*nx+ix]=dt*mu[iy*nx+ix]*sxy1+sxy[iy*nx+ix];

 if(ix<NPAD){
  sx[iy*nx+ix]*=xytaper[ix];
  sy[iy*nx+ix]*=xytaper[ix];
  sxy[iy*nx+ix]*=xytaper[ix];
  sx[iy*nx+nx-1-ix]*=xytaper[ix];
  sy[iy*nx+nx-1-ix]*=xytaper[ix]; 
  sxy[iy*nx+nx-1-ix]*=xytaper[ix]; 
 }
 
 if(iy<NPAD){
  sx[iy*nx+ix]*=xytaper[iy];
  sy[iy*nx+ix]*=xytaper[iy];
  sxy[iy*nx+ix]*=xytaper[iy];
  sx[(ny-1-iy)*nx+ix]*=xytaper[iy];
  sy[(ny-1-iy)*nx+ix]*=xytaper[iy]; 
  sxy[(ny-1-iy)*nx+ix]*=xytaper[iy];
 }
}


