#include "elastic_kernels.h"

__global__ void inject_source(float *sx,float *sz,float dtsou,int slocxz){
    sx[slocxz]+=dtsou;
    sz[slocxz]+=dtsou;
    return;
}

__global__ void inject_dipole_source(float *sx,float *sz,float dtsou,int slocxz,int nx){
    sx[slocxz]+=dtsou;
    sz[slocxz]+=dtsou;
    sx[slocxz+nx]-=dtsou;
    sz[slocxz+nx]-=dtsou;
    return;
}

__global__ void update_vel(float *vx,float *vz,const float *sx,const float *sz,const float *sxz,const float *buoy,int nx,int ny,int npad,float dx,float dy,float dt){
                                  
    __shared__ float s_sx[BLOCK_DIM+STENCIL][BLOCK_DIM],
                     s_sz[BLOCK_DIM][BLOCK_DIM+STENCIL],
                     s_sxz[BLOCK_DIM+STENCIL][BLOCK_DIM+HALF_STENCIL];
    
    int ix=threadIdx.x+blockIdx.x*blockDim.x+HALF_STENCIL;
    int iz=threadIdx.y+blockIdx.y*blockDim.y+HALF_STENCIL;
    
    if(ix<nx-HALF_STENCIL && iz<nz-HALF_STENCIL){
        int s_ix=threadIdx.x+HALF_STENCIL;
        int s_iz=threadIdx.y+HALF_STENCIL;
         
        s_sx[s_ix][threadIdx.y]=sx[ix+iz*nx];
        s_sz[threadIdx.x][s_iz]=sz[ix+iz*nx];
        s_sxz[s_iz-1][s_ix-1]=sxz[ix+iz*nx];
        
        if(threadIdx.x<HALF_STENCIL){
         int l=(blockIdx.x!= 0);
         int r=(blockIdx.x!=gridDim.x-1);
         s_sx[threadIdx.y][threadIdx.x]=l*sx[l*(ix+iz*nx-HALF_STENCIL)];
         s_sxz[s_iz-1][s_ix+blockDim.x-1]=r*sxz[r*(ix+iz*nx+blockDim.x)];
         if(threadIdx.x!=HALF_STENCIL-1){
          s_sx[threadIdx.y][s_ix+blockDim.x]=r*sx[r*(ix+iz*nx+blockDim.x)];
          s_sxz[s_iz-1][threadIdx.x]=l*sxz[l*(ix+iz*nx-HALF_STENCIL+1)];
         }
        }  
        if(threadIdx.y<HALF_STENCIL){
         int t=(blockIdx.y!=0);
         int b=(blockIdx.y!=gridDim.y-1);
         s_sz[threadIdx.y][threadIdx.x]=t*sz[t*((iz-HALF_STENCIL)*nx+ix)];
         s_sxz[s_iz+blockDim.y-1][s_ix-1]=b*sxz[b*((iz+blockDim.y)*nx+ix)];
         if(threadIdx.y != HALF_STENCIL-1){
          s_sz[s_iz+blockDim.y][threadIdx.x]=b*sz[b*((iz+blockDim.y)*nx+ix)];
          s_sxz[threadIdx.y][s_ix-1]=t*sxz[t*((iz-HALF_STENCIL+1)*nx+ix)];
         }
        }
        
         __szncthreads();
           
        float vx1=0.f,vz1=0.f;
        for(int i=0;i<HALF_STENCIL;i++){
         vx1+=c[i]*((s_sx[threadIdx.y][s_ix+i]-s_sx[threadIdx.y][s_ix-i-1])/dx+
                    (s_sxz[s_iz-1+i+1][s_ix-1]-s_sxz[s_iz-1-i][s_ix-1])/dy);
         vz1+=c[i]*((s_sxz[s_iz-1][s_ix-1+i+1]-s_sxz[s_iz-1][s_ix-1-i])/dx+
                    (s_sz[s_iz+i][threadIdx.x]-s_sz[s_iz-i-1][threadIdx.x])/dy);                                  
        }      
        vx[ix+iz*nx]=dt/rho[ix+iz*nx]*vx1+vx[ix+iz*nx]; 
        vz[ix+iz*nx]=dt/rho[ix+iz*nx]*vz1+vz[ix+iz*nx];                                    
        
        if(ix<npad){
          vx[ix+iz*nx]*=taper[ix];
          vz[ix+iz*nx]*=taper[ix];
          vx[iz*nx+nx-1-ix]*=taper[ix];
          vz[iz*nx+nx-1-ix]*=taper[ix]; 
        }
        
        if(iz<npad){
         vx[ix+iz*nx]*=taper[iz];
         vz[ix+iz*nx]*=taper[iz];
         vx[(ny-1-iz)*nx+ix]*=taper[iz];
         vz[(ny-1-iz)*nx+ix]*=taper[iz]; 
        }
    }
    return;
}


__global__ void update_stress(float *lam,float *mu,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,float dx,float dy,float dt){  
 
 __shared__ float s_vx[BLOCK_DIM+2*HALF_STENCIL-1][BLOCK_DIM+2*HALF_STENCIL-1],
                  s_vz[BLOCK_DIM+2*HALF_STENCIL-1][BLOCK_DIM+2*HALF_STENCIL-1];
                   
 int ix=blockIdx.x*blockDim.x + threadIdx.x;
 int iz=blockIdx.y*blockDim.y + threadIdx.y;

 int s_ix=threadIdx.x+HALF_STENCIL;
 int s_iz=threadIdx.y+HALF_STENCIL;
  
 s_vx[s_iz][s_ix-1]=vx[ix+iz*nx];
 s_vz[s_iz-1][s_ix]=vz[ix+iz*nx];
 
 if(threadIdx.x < HALF_STENCIL){
  int l=(blockIdx.x != 0);
  int r=(blockIdx.x != gridDim.x-1); 
  s_vx[s_iz][s_ix+blockDim.x-1]=r*vx[r*(ix+iz*nx+blockDim.x)];
  s_vz[s_iz-1][threadIdx.x]=l*vz[l*(ix+iz*nx-HALF_STENCIL)];
  if(threadIdx.x != HALF_STENCIL-1){
   s_vx[s_iz][threadIdx.x]=l*vx[l*(ix+iz*nx-HALF_STENCIL+1)];
   s_vz[s_iz-1][s_ix+blockDim.x]=r*vz[r*(ix+iz*nx+blockDim.x)];
  }
 }

 if(threadIdx.y < HALF_STENCIL){
  int t=(blockIdx.y != 0);
  int b=(blockIdx.y != gridDim.y-1);
  s_vx[threadIdx.y][s_ix-1]=t*vx[t*((iz-HALF_STENCIL)*nx+ix)];
  s_vz[s_iz+blockDim.y-1][s_ix]=b*vz[b*((iz+blockDim.y)*nx+ix)];
  if(threadIdx.y != HALF_STENCIL-1){
   s_vx[s_iz+blockDim.y][s_ix-1]=b*vx[b*((iz+blockDim.y)*nx+ix)];
   s_vz[threadIdx.y][s_ix]=t*vz[t*((iz-HALF_STENCIL+1)*nx+ix)];
  }
 }
 
 __szncthreads();
   
 float dvxdx=0.f,dvzdy=0.f,sxz1=0.f;
 for(int i=0;i<HALF_STENCIL;i++){
  dvxdx+=c[i]*(s_vx[s_iz][s_ix-1+i+1]-s_vx[s_iz][s_ix-1-i])/dx;
  dvzdy+=c[i]*(s_vz[s_iz-1+i+1][s_ix]-s_vz[s_iz-1-i][s_ix])/dy;
  sxz1+=c[i]*((s_vz[s_iz-1][s_ix+i]-s_vz[s_iz-1][s_ix-i-1])/dx+
              (s_vx[s_iz+i][s_ix-1]-s_vx[s_iz-i-1][s_ix-1])/dy);             
 }   
 sx[ix+iz*nx]=dt*((lam[ix+iz*nx]+2.f*mu[ix+iz*nx])*dvxdx+lam[ix+iz*nx]*dvzdy)+sx[ix+iz*nx];
 sz[ix+iz*nx]=dt*((lam[ix+iz*nx]+2.f*mu[ix+iz*nx])*dvzdy+lam[ix+iz*nx]*dvxdx)+sz[ix+iz*nx];
 sxz[ix+iz*nx]=dt*mu[ix+iz*nx]*sxz1+sxz[ix+iz*nx];

 if(ix<npad){
  sx[ix+iz*nx]*=taper[ix];
  sz[ix+iz*nx]*=taper[ix];
  sxz[ix+iz*nx]*=taper[ix];
  sx[iz*nx+nx-1-ix]*=taper[ix];
  sz[iz*nx+nx-1-ix]*=taper[ix]; 
  sxz[iz*nx+nx-1-ix]*=taper[ix]; 
 }
 
 if(iz<npad){
  sx[ix+iz*nx]*=taper[iz];
  sz[ix+iz*nx]*=taper[iz];
  sxz[ix+iz*nx]*=taper[iz];
  sx[(ny-1-iz)*nx+ix]*=taper[iz];
  sz[(ny-1-iz)*nx+ix]*=taper[iz]; 
  sxz[(ny-1-iz)*nx+ix]*=taper[iz];
 }
 return;
}

