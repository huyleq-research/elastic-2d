#ifndef ELASTIC_KERNELS_H
#define ELASTIC_KERNELS_H

#define BLOCK_DIM 16
#define HALF_STENCIL 4
#define STENCIL 8

#define c1 1.1962890625
#define c2 -0.07975260416
#define c3 0.0095703125
#define c4 -0.00069754464

#define a1 1.27323954474
#define a2 -0.141471060526
#define a3 0.0509295817894
#define a4 -0.0259844805048

__global__ void inject_source(float *sx,float *sz,float dtsou,int slocxz);

__global__ void inject_dipole_source(float *sx,float *sz,float dtsou,int slocxz,int nx);

__global__ void update_vel(float *rho,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,float dx,float dy,float dt);

__global__ void update_stress(float *lam,float *mu,float *sx,float *sz,float *sxz,float *vx,float *vz,int nx,int ny,int npad,float dx,float dy,float dt);

#endif
